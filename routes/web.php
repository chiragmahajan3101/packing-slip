<?php

use App\Http\Controllers\OrderEntriesController;
use App\Http\Controllers\OrderGroupsController;
use App\Http\Controllers\PartyInfosController;
use App\Http\Controllers\SlipsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


Route::delete('partyInfos/trash/{partyInfo}', [PartyInfosController::class, 'trash'])->name('partyInfos.trash')->middleware(['auth']);
Route::get('partyInfos/trashed', [PartyInfosController::class, 'trashed'])->name('partyInfos.trashed')->middleware(['auth']);
Route::put('partyInfos/restore/{partyInfo}', [PartyInfosController::class, 'restore'])->name('partyInfos.restore')->middleware(['auth']);
Route::resource('partyInfos', PartyInfosController::class)->middleware(['auth']);
Route::resource('slips', SlipsController::class)->middleware(['auth']);
Route::resource('slips.orderGroups', OrderGroupsController::class)->except(['index', 'show'])->middleware(['auth']);
Route::resource('orderGroups.orderEntries', OrderEntriesController::class)->except(['index', 'show'])->middleware(['auth']);


require __DIR__ . '/auth.php';
