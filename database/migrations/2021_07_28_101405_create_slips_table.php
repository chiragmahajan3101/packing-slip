<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slips', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('party_info_id');

            $table->unsignedInteger('serial_no');
            $table->timestamp('bill_date')->nullable();
            $table->string('LR_NO');
            $table->string('delivery_at');
            $table->unsignedInteger('year');
            $table->string('quality');

            $table->unsignedInteger('total_piece')->default(0);
            $table->unsignedFloat('total_meter')->default(0);
            $table->unsignedFloat('actual_meter')->default(0);
            $table->unsignedInteger('fold')->nullable();
            $table->unique(['year', 'serial_no']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('party_info_id')->references('id')->on('party_infos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slips');
    }
}
