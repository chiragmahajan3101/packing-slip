<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('party_infos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('company_name');
            $table->string('company_address');
            $table->unsignedBigInteger('contact_no')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('GST_no')->unique();
            $table->string('state');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('party_infos');
    }
}
