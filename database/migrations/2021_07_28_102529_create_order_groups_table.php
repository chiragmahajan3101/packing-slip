<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_groups', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedInteger('bale_no');
            $table->unsignedInteger('order_group_year')->nullable();
            $table->unsignedBigInteger('slip_id');
            $table->unsignedFloat('group_total_meter')->default(0);
            $table->unique(['order_group_year', 'bale_no']);

            $table->foreign('slip_id')->references('id')->on('slips')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_groups');
    }
}
