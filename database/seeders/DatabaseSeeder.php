<?php

namespace Database\Seeders;

use App\Models\OrderEntry;
use App\Models\OrderGroup;
use App\Models\PartyInfo;
use App\Models\Slip;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Chirag Mahajan',
            'email' => 'chiragmahajan3101@gmail.com',
            'password' => Hash::make('chirag3101'),
        ]);
        PartyInfo::factory(5)->create()->each(function (PartyInfo $partyInfo) {
            $partyInfo->partySlips()->saveMany(
                Slip::factory(rand(2, 3))->make()
            )->each(function (Slip $slip) {
                $slip->slipGroups()->saveMany(
                    OrderGroup::factory(8)->make()
                )->each(function (OrderGroup $orderGroup) {
                    $orderGroup->baleEntries()->saveMany(
                        OrderEntry::factory(rand(5, 6))->make()
                    );
                });
            });
        });
    }
}
