<?php

namespace Database\Factories;

use App\Models\PartyInfo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PartyInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     * @var string
     */
    protected $model = PartyInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->name(),
            'company_address' => $this->faker->address(),
            'contact_no' => $this->faker->randomNumber(8, true),
            'email' => $this->faker->unique()->safeEmail(),
            'GST_no' => Str::random(15),
            'state' => $this->faker->country(),
        ];
    }
}
