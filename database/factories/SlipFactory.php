<?php

namespace Database\Factories;

use App\Models\Slip;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SlipFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Slip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'serial_no' => $this->faker->unique()->numberBetween(1, 100),
            'bill_date' => $this->faker->date('Y-m-d', 'now'),
            'LR_NO' => Str::random(7),
            'delivery_at' => $this->faker->address(),
            'transport_name' => Str::random(4),
            'quality' => Str::random(7),
            'fold' => $this->faker->numberBetween(1, 40),
        ];
    }
}
