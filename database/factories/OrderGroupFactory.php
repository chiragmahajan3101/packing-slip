<?php

namespace Database\Factories;

use App\Models\OrderGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bale_no' => $this->faker->unique()->numberBetween(1, 3900),
        ];
    }
}
