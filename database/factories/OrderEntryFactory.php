<?php

namespace Database\Factories;

use App\Models\OrderEntry;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OrderEntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderEntry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'loom_no' => $this->faker->numberBetween(1, 24),
            'piece_no' => Str::random(6),
            'meter' => $this->faker->randomFloat(2, 1, 150),
        ];
    }
}
