<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderEntry\CreateOrderEntryRequest;
use App\Http\Requests\OrderEntry\UpdateOrderEntryRequest;
use App\Models\OrderEntry;
use App\Models\OrderGroup;
use Illuminate\Http\Request;


class OrderEntriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(OrderGroup $orderGroup)
    {
        return view('orderEntry.create', compact(['orderGroup']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrderEntryRequest $request, OrderGroup $orderGroup)
    {
        $orderGroup->baleEntries()->create([
            'loom_no' => (int)$request->loom_no,
            'piece_no' => $request->piece_no,
            'meter' => (float)$request->meter,
        ]);
        session()->flash('success', 'Entry has been made successfully!');
        return redirect(route('orderGroups.orderEntries.create', $orderGroup->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderEntry  $orderEntry
     * @return \Illuminate\Http\Response
     */
    public function show(OrderEntry $orderEntry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderEntry  $orderEntry
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderGroup $orderGroup, OrderEntry $orderEntry)
    {
        return view('orderEntry.edit', compact(['orderGroup', 'orderEntry']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderEntry  $orderEntry
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderEntryRequest $request, OrderGroup $orderGroup, OrderEntry $orderEntry)
    {
        $difference = $request->meter - $orderEntry->meter;
        $orderEntry->update([
            'loom_no' => (int)$request->loom_no,
            'piece_no' => $request->piece_no,
            'meter' => (float)$request->meter,
        ]);
        if ($difference != 0) {
            $orderGroup->update([
                'group_total_meter' => $orderGroup->group_total_meter + $difference,
            ]);
            $orderGroup->slip->update([
                'total_meter' => $orderGroup->slip->total_meter + $difference
            ]);
        }
        session()->flash('success', 'Entry has been Updated successfully!');
        return redirect(route('orderGroups.orderEntries.create', $orderGroup->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderEntry  $orderEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderGroup $orderGroup, OrderEntry $orderEntry)
    {
        $orderEntry->delete();
        session()->flash('success', 'Entry has been Deleted successfully!');
        return redirect(route('orderGroups.orderEntries.create', $orderGroup->id));
    }
}
