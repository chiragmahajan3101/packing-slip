<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartyInfo\CreatePartyInfoRequest;
use App\Http\Requests\PartyInfo\UpdatePartyInfoRequest;
use App\Models\PartyInfo;
use Illuminate\Http\Request;

class PartyInfosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parties = PartyInfo::orderBy('company_name')->paginate(3);
        return view('partyAddress.index', compact(['parties']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partyAddress.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePartyInfoRequest $request)
    {
        PartyInfo::create([
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'contact_no' => $request->contact_no,
            'email' => $request->email,
            'GST_no' => $request->GST_no,
            'state' => $request->state,
        ]);

        session()->flash('success', 'Party has been added successfully!');
        return redirect(route('partyInfos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PartyInfo  $partyInfo
     * @return \Illuminate\Http\Response
     */
    public function show(PartyInfo $partyInfo)
    {
        return view('partyAddress.show', compact(['partyInfo']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PartyInfo  $partyInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(PartyInfo $partyInfo)
    {
        return view('partyAddress.edit', compact(['partyInfo']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PartyInfo  $partyInfo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePartyInfoRequest $request, PartyInfo $partyInfo)
    {
        $partyInfo->update([
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'contact_no' => $request->contact_no,
            'email' => $request->email,
            'GST_no' => $request->GST_no,
            'state' => $request->state,
        ]);
        session()->flash('success', 'Party has been updated successfully!');
        return redirect(route('partyInfos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PartyInfo  $partyInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partyInfo = PartyInfo::onlyTrashed()->findOrFail($id);
        $partyInfo->forceDelete();
        session()->flash('success', 'Party has been deleted successfully!');
        return redirect(route('partyInfos.index'));
    }

    public function trash(PartyInfo $partyInfo)
    {
        $partyInfo->delete();
        session()->flash('success', 'Party has been trashed successfully!');
        return redirect(route('partyInfos.index'));
    }

    public function trashed()
    {
        $trashed = PartyInfo::onlyTrashed()->orderBy('company_name')->paginate(2);
        return view('partyAddress.trashed', ['parties' => $trashed]);
    }

    public function restore($id)
    {
        $trashedParty = PartyInfo::onlyTrashed()->findOrFail($id);
        $trashedParty->restore();
        session()->flash('success', 'Party restored successfully!');
        return redirect(route('partyInfos.index'));
    }
}
