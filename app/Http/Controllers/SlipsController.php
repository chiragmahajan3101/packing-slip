<?php

namespace App\Http\Controllers;

use App\Http\Requests\Slip\CreateSlipRequest;
use App\Http\Requests\Slip\UpdateSlipRequest;
use App\Models\OrderGroup;
use App\Models\Slip;
use App\Models\PartyInfo;
use Illuminate\Http\Request;

class SlipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slips = Slip::search()->orderBy('bill_date')->paginate(4);
        $partyInfos = PartyInfo::orderBy('company_name')->get();
        return view('slip.index', compact(['slips', 'partyInfos']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slip = Slip::orderBy('year', 'DESC')->orderBy('serial_no', 'DESC')->first();
        $partyInfos = PartyInfo::orderBy('company_name')->get();
        return view('slip.create', compact(['slip', 'partyInfos']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSlipRequest $request)
    {
        Slip::create([
            'party_info_id' => $request->party_id,
            'serial_no' => $request->serial_no,
            'bill_date' => $request->bill_date,
            'LR_NO' => $request->LR_NO,
            'delivery_at' => $request->delivery_at,
            'quality' => $request->quality,
            'user_id' => auth()->id(),
            'fold' => (int)$request->fold,
        ]);

        $slip = Slip::orderBy('id', 'DESC')->first();
        session()->flash('success', 'Slip has been created successfully!');
        return redirect(route('slips.orderGroups.create', $slip->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slip  $slip
     * @return \Illuminate\Http\Response
     */
    public function show(Slip $slip)
    {
        return view('slip.show', compact(['slip']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slip  $slip
     * @return \Illuminate\Http\Response
     */
    public function edit(Slip $slip)
    {
        $partyInfos = PartyInfo::orderBy('company_name')->get();
        return view('slip.edit', compact(['slip', 'partyInfos']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slip  $slip
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlipRequest $request, Slip $slip)
    {
        $slip->update([
            'party_info_id' => $request->party_id,
            'serial_no' => $request->serial_no,
            'bill_date' => $request->bill_date,
            'LR_NO' => $request->LR_NO,
            'delivery_at' => $request->delivery_at,
            'quality' => $request->quality,
            'user_id' => auth()->id(),
            'fold' => (int)$request->fold,
        ]);
        $ids = $slip->slipGroups->pluck('id')->toArray();
        OrderGroup::whereIn('id', $ids)->update([
            'order_group_year' => $slip->year,
        ]);
        session()->flash('success', 'Slip detail has been updated successfully!');
        return redirect(route('slips.orderGroups.create', $slip->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slip  $slip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slip $slip)
    {
        $slip->delete();
        session()->flash('success', 'Slip has been deleted successfully!');
        return redirect(route('slips.index'));
    }
}
