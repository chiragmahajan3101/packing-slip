<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderGroup\CreateOrderGroupRequest;
use App\Models\OrderGroup;
use App\Models\Slip;
use Illuminate\Http\Request;

class OrderGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Slip $slip)
    {
        $orderGroup = OrderGroup::orderBy('order_group_year', 'DESC')->orderBy('bale_no', 'DESC')->first();
        return view('orderGroup.create', compact(['slip', 'orderGroup']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrderGroupRequest $request, Slip $slip)
    {
        $slip->slipGroups()->create([
            'bale_no' => $request->bale_no
        ]);

        $orderGroup = OrderGroup::orderBy('id', 'DESC')->first();
        session()->flash('success', 'Bale has been created successfully!');
        return redirect(route('orderGroups.orderEntries.create', $orderGroup->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderGroup  $orderGroup
     * @return \Illuminate\Http\Response
     */
    public function show(OrderGroup $orderGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderGroup  $orderGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Slip $slip, OrderGroup $orderGroup)
    {
        return view('orderGroup.edit', compact(['orderGroup', 'slip']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderGroup  $orderGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slip $slip, OrderGroup $orderGroup)
    {
        $orderGroup->update([
            'bale_no' => $request->bale_no,
        ]);
        session()->flash('success', 'Bale has Updated created successfully!');
        return redirect(route('orderGroups.orderEntries.create', compact(['orderGroup'])));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderGroup  $orderGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slip $slip, OrderGroup $orderGroup)
    {
        $piece = sizeOf($orderGroup->baleEntries->pluck('id')->toArray());
        $orderGroup->slip->update([
            'total_piece' => $orderGroup->slip->total_piece - $piece,
        ]);
        $orderGroup->delete();
        session()->flash('success', 'Bale has Deleted successfully!');
        return redirect(route('slips.orderGroups.create', compact(['slip'])));
    }
}
