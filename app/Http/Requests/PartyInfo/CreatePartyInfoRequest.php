<?php

namespace App\Http\Requests\PartyInfo;

use Illuminate\Foundation\Http\FormRequest;

class CreatePartyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|max:255',
            'company_address' => 'required',
            'contact_no' => '',
            'email' => '',
            'GST_no' => 'required|unique:party_infos,GST_no|max:15',
            'state' => 'required|max:20',
        ];
    }
}
