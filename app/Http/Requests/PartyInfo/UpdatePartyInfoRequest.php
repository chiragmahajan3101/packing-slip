<?php

namespace App\Http\Requests\PartyInfo;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePartyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|max:255',
            'company_address' => 'required',
            'contact_no' => 'max:10|min:10',
            'email' => '',
            'GST_no' => 'required|max:15|unique:party_infos,GST_no,' . $this->partyInfo->id,
            'state' => 'required|max:20',
        ];
    }
}
