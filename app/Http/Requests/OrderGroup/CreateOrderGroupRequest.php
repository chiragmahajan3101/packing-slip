<?php

namespace App\Http\Requests\OrderGroup;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bale_no' => 'required',
        ];
    }
}
