<?php

namespace App\Http\Requests\OrderEntry;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderEntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loom_no' => 'required',
            'piece_no' => 'required',
            'meter' => 'required|min:0',
        ];
    }
}
