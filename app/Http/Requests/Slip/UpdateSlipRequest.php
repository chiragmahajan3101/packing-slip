<?php

namespace App\Http\Requests\Slip;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSlipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'party_id' => 'required|exists:party_infos,id',
            'serial_no' => 'required',
            'bill_date' => 'required',
            'LR_NO' => 'required',
            'delivery_at' => 'required',
            'quality' => 'required',
            'fold' => 'required|min:0',
        ];
    }
}
