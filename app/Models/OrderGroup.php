<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderGroup extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();
        static::created(function (OrderGroup $orderGroup) {
            $orderGroup->order_group_year = $orderGroup->slip->year;
            $orderGroup->save();
        });
        static::deleted(function (OrderGroup $orderGroup) {
            $meter = $orderGroup->group_total_meter;
            $orderGroup->slip->update([
                'total_meter' => $orderGroup->slip->total_meter - $meter,
            ]);
        });
    }

    public function setBaleNoAttribute($value)
    {
        $this->attributes['bale_no'] = (int)$value;
        //$this->attributes['order_group_year'] = $this->slip->year;
    }

    public function slip()
    {
        return $this->belongsTo(Slip::class, 'slip_id');
    }

    public function baleEntries()
    {
        return $this->hasMany(OrderEntry::class);
    }
}
