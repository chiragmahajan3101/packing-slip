<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderEntry extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();
        static::created(function (OrderEntry $orderEntry) {
            $orderEntry->baleGroup->slip->total_meter = $orderEntry->baleGroup->slip->total_meter + $orderEntry->meter;
            $orderEntry->baleGroup->group_total_meter = $orderEntry->baleGroup->group_total_meter + $orderEntry->meter;
            $orderEntry->baleGroup->slip->increment('total_piece');
            $orderEntry->save();
            $orderEntry->baleGroup->save();
            $orderEntry->baleGroup->slip->save();
        });
        static::deleted(function (OrderEntry $orderEntry) {
            $meter = $orderEntry->meter;
            $orderEntry->baleGroup->slip->update([
                'total_meter' => $orderEntry->baleGroup->slip->total_meter - $meter,
                'total_piece' => $orderEntry->baleGroup->slip->total_piece - 1,
            ]);
            $orderEntry->baleGroup->update([
                'group_total_meter' => $orderEntry->baleGroup->group_total_meter - $meter
            ]);
        });
    }
    public function baleGroup()
    {
        return $this->belongsTo(OrderGroup::class, 'order_group_id');
    }

    public function scopeUpdateSum()
    {
        $this->baleGroup->slip->total_meter = $this->baleGroup->slip->total_meter - $this->meter;
        $this->baleGroup->slip->save();
        $this->baleGroup->group_total_meter = $this->baleGroup->group_total_meter - $this->meter;
        $this->baleGroup->save();
    }
}
