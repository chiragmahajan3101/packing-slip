<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slip extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    /**
     * Mutators
     */

    public function setBillDateAttribute($bill_date)
    {
        $this->attributes['bill_date'] = (new Carbon($bill_date))->format('y/m/d');
        $this->attributes['year'] = (int)explode('-', $bill_date)[0];
    }


    public function party()
    {
        return $this->belongsTo(PartyInfo::class, 'party_info_id');
    }

    public function slipGroups()
    {
        return $this->hasMany(OrderGroup::class);
    }

    public function scopeSearch($query)
    {
        $year = request('year');
        $party = request('party_id');
        $serial = request('serial_No');
        if ($year) {
            if ($party) {
                if ($serial) {
                    return $query->where('year', $year)->where('serial_no', $serial)->where('party_info_id', $party);
                }
                return $query->where('year', $year)->where('party_info_id', $party);
            }
            return $query->where('year', $year);
        }
        if ($party) {
            if ($serial) {
                return $query->where('serial_no', $serial)->where('party_info_id', $party);
            }
            return $query->where('party_info_id', $party);
        }
        if ($serial)
            return $query->where('serial_no', $serial);
    }

    public function calculateAMeter()
    {
        $this->actual_meter = ($this->total_meter - ((100 - $this->fold) * ($this->total_meter)) / 100);
        $this->save();
    }
}
