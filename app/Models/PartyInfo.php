<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartyInfo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id'];

    public function partySlips()
    {
        return $this->hasMany(Slip::class)->orderBy('bill_date');
    }

    public function getSlipCountAttribute()
    {
        return $this->partySlips()->count() ? 'btn-success' : 'btn-danger';
    }
}
