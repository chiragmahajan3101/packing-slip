@extends('layouts.billGeneration.app')

@section('content')
<div class="card mb-2 col-md-5">
    <div class="card-header"><h2>Edit Entry</h2></div>
        <div class="card-body">
            <form action="{{route('orderGroups.orderEntries.update', [$orderGroup->id, $orderEntry->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="bale_no">Loom No</label>
                    <input type="number"
                            class="form-control @error('loom_no') is-invalid @enderror"
                            name="loom_no"
                            value="{{ old('loom_no', $orderEntry->loom_no) }}"
                            placeholder="Enter Loom Number"
                            id="loom_no">
                    @error('loom_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="piece_no">Piece No</label>
                    <input type="text"
                            class="form-control @error('piece_no') is-invalid @enderror"
                            name="piece_no"
                            value="{{ old('piece_no', $orderEntry->piece_no) }}"
                            placeholder="Enter Piece Number"
                            id="piece_no">
                    @error('piece_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="meter">Meter</label>
                    <input type="number"
                            step="0.01"
                            class="form-control @error('meter') is-invalid @enderror"
                            name="meter"
                            value="{{ old('meter', $orderEntry->meter) }}"
                            placeholder="Enter Meter"
                            id="meter">
                    @error('meter')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
