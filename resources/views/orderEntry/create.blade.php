@extends('layouts.billGeneration.app')

@section('content')
<div class="card mb-2 col-md-5">
    <div class="card-header"><h2>Create New Entry</h2></div>
        <div class="card-body">
            <form action="{{route('orderGroups.orderEntries.store', $orderGroup->id)}}" method="POST">
                @csrf

                <div class="form-group">
                    <label for="bale_no">Loom No</label>
                    <input type="number"
                            class="form-control @error('loom_no') is-invalid @enderror"
                            name="loom_no"
                            value="{{ old('loom_no') }}"
                            placeholder="Enter Loom Number"
                            id="loom_no">
                    @error('loom_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="piece_no">Piece No</label>
                    <input type="text"
                            class="form-control @error('piece_no') is-invalid @enderror"
                            name="piece_no"
                            value="{{ old('piece_no') }}"
                            placeholder="Enter Piece Number"
                            id="piece_no">
                    @error('piece_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="meter">Meter</label>
                    <input type="number"
                            step="0.01"
                            class="form-control @error('meter') is-invalid @enderror"
                            name="meter"
                            value="{{ old('meter') }}"
                            placeholder="Enter Meter"
                            id="meter">
                    @error('meter')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>
@if($orderGroup->baleEntries->count())
<div class="card mx-auto mb-2 col-md-12">
    <a href="{{route('slips.orderGroups.create', $orderGroup->slip->id)}}" class="btn btn-success mt-2">Complete This Bale</a>
    <div class="card-header"><h2>Existing Entries</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Loom No</th>
                        <th>Piece No</th>
                        <th>Meter</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orderGroup->baleEntries as $orderEntry)
                        <tr>
                            <td>{{$orderEntry->loom_no}}</td>
                            <td>{{$orderEntry->piece_no}}</td>
                            <td>{{$orderEntry->meter}}</td>
                            <td>
                                <a href="{{route('orderGroups.orderEntries.edit', [$orderGroup->id, $orderEntry->id])}}" class="btn btn-sm btn-primary mb-1">
                                    Edit
                                </a>
                                <form action="{{route('orderGroups.orderEntries.destroy', [$orderGroup->id, $orderEntry->id])}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger">Trash</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
@endsection
