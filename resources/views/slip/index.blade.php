@extends('layouts.billGeneration.app')

@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="card mx-auto col-md-10 p-2 mt-2 mb-2">
    <a href="{{route('slips.create')}}" class="btn btn-primary">Create Slip</a>
    <div class="card-header">
        <h4 class="text-center">Search</h4>
    </div>
    <div class="card-body">
        <form action="{{route('slips.index')}}" method="GET">
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="year">Year</label>
                    <input type="year"
                            class="form-control"
                            name="year"
                            value="{{ request('year')}}"
                            placeholder="e.g. 2019">
                </div>
                <div class="form-group col-md-6">
                    <label for="party_id">Party</label>
                    <select name="party_id" id="party_id" class="form-control select2">
                        <option></option>
                        @foreach ($partyInfos as $partyInfo)
                            @if($partyInfo->id == old('party_id'))
                                <option value="{{$partyInfo->id}}" selected>{{$partyInfo->company_name}}</option>
                            @else
                                <option value="{{$partyInfo->id}}">{{$partyInfo->company_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 ml-4">
                    <label for="serial_No">Serial Number</label>
                    <input type="number"
                            class="form-control d-inline"
                            name="serial_No"
                            value="{{ request('serial_no')}}"
                            placeholder="e.g. 29">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">
                Search
            </button>
        </form>
    </div>
</div>
@endsection

@section('slip-table')
<div class="card mx-auto mt-2 mb-2">
    <div class="card-body">
        @if(!$slips->count())
        No Results Found
        @endif
        @foreach ($slips as $slip)
        <a href="{{route('slips.show', $slip->id)}}" class="btn btn-warning mb-2 mr-2">
            <div class="d-inline">
                <table class="table d-inline">
                    <tr>
                        <th>Party</th>
                        <th>Serial No</th>
                        <th>Slip Date</th>
                        <th>Meter</th>
                        <th>Taga</th>
                    </tr>
                    <tr>
                        <td>{{$slip->party->company_name}}</td>
                        <td>{{$slip->serial_no}}</td>
                        <td>{{explode(" ",$slip->bill_date)[0]}}</td>
                        <td>{{$slip->total_meter}} </td>
                        <td>{{ $slip->total_piece}}</td>
                    </tr>
                </table>
            </div>
        </a>
        @endforeach
    </div>
    <div class="ml-3">
        {{ $slips->links('vendor.pagination.bootstrap-4') }}
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select an option',
        allowClear: true
    });
</script>
@endsection
