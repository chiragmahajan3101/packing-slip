@extends('layouts.billGeneration.app')

@section('content')
<div class="card mx-auto col-md-12 p-2 mt-2 mb-2" id="printable">
    <a href="{{route('slips.edit', $slip->id)}}" class="btn btn-primary">Edit</a>
    <button type="submit" class="btn btn-danger" onclick="displayModal({{ $slip->id }})" data-toggle="modal" data-target="#deleteModal">Delete</button>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Slip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteSlipForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        Are you sure, you want to Delete this Slip?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delete Slip</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card-header">
            <div class="d-flex justify-content-between">
                <div></div>
                <div id="head">|| Shri Ganeshji Prasanna ||</div>
                <div class="d-flex flex-column align-items-end" id="contact">
                    <div>Off:2441177</div>
                    <div>cell:9423901111</div>
                </div>
            </div>
            <div class="d-flex justify-content-center"><strong>PACKING SLIP</strong></div>
            <div class="d-flex justify-content-center"><h4>SHRI SAI DARSHAN FABRICS</h4></div>
            <div class="d-flex justify-content-center">22/635, Ganesh Nagar, Near Sawant Process,</div>
            <div class="d-flex justify-content-center">ICHALKARANJI - 416115 Dist:-Kolhapur (Maharashtra)</div>
    </div>
    <div class="card-body">
        <div class="d-flex">
            <div class="mr-auto">No. {{$slip->serial_no}}</div>
            <div>Date :- {{explode(" ", $slip->bill_date)[0]}}</div>
        </div>
        <div>
            To - <strong>{{$slip->party->company_name}}</strong>
        </div>
        <div class="d-flex">
            <div class="mr-auto">Tempo No. / L.R. No.- <strong>{{$slip->LR_NO}}</strong></div>
            <div>Address- <strong>{{$slip->party->company_address}}</strong></div>
        </div>
        <div class="d-flex">
            <div class="mr-auto">Delivery At.- <strong>{{$slip->delivery_at}}</strong></div>
            <div>Quality- <strong>{{$slip->quality}}</strong></div>
        </div>
        <div class="d-flex">
            <div class="mr-auto">GST- {{$slip->party->GST_no}}</div>
            <div>State- <strong>{{$slip->party->state}}</strong></div>
        </div>
        <div class="d-flex justify-content-center border border-dark flex-row mt-2">
            <div class="d-none">{{$i=0}}{{$row=0}}</div>
            <div class="">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Loom No</th>
                            <th>Piece No.</th>
                            <th>Meter</th>
                            <th>Bale No.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($slip->slipGroups as $index=>$slipGroup)
                        @foreach ($slipGroup->baleEntries as $key=>$baleEntry)
                        @if ($row >= 30)
                            @break;
                        @endif
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{ $baleEntry->loom_no }}</td>
                            <td>{{$baleEntry->piece_no}}</td>
                            <td>{{$baleEntry->meter}}</td>
                            <div class="d-none">{{++$row}}</div>
                            @if (++$key == round($slipGroup->baleEntries->count()/2))
                            <td><strong>{{$slipGroup->bale_no}}</strong></td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                        @if($row >= 30)
                        @break
                        @else
                        <tr>
                            <td colspan="3"></td>
                            <div class="d-none">{{++$row}}</div>
                            <td><strong>{{$slipGroup->group_total_meter}}</strong></td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>

                </table>
            </div>
            <div class="d-none">{{$rows=0}}</div>
            <div class="">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Loom No</th>
                            <th>Piece No.</th>
                            <th>Meter</th>
                            <th>Bale No.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($slip->slipGroups as $index=>$slipGroup)
                        @foreach ($slipGroup->baleEntries as $key=>$baleEntry)
                        @if ($rows <= 29)
                            <div class="d-none">{{++$rows}}</div>
                        @else
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{ $baleEntry->loom_no }}</td>
                            <td>{{$baleEntry->piece_no}}</td>
                            <td>{{$baleEntry->meter}}</td>
                            <div class="d-none">{{++$rows}}</div>
                            @if (++$key == round($slipGroup->baleEntries->count()/2))
                            <td><strong>{{$slipGroup->bale_no}}</strong></td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endif
                        @endforeach
                        @if($rows <= 29)
                        <div class="d-none">{{++$rows}}</div>
                        @else
                        <tr>
                            <td colspan="3"></td>
                            <div class="d-none">{{++$rows}}</div>
                            <td><strong>{{$slipGroup->group_total_meter}}</strong></td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex flex-row">
            <div class="d-flex flex-column">
                <div class="">Total Taga: {{$slip->total_piece}} Taga</div>
                <div class="">Fold: {{$slip->fold}}</div>
            </div>
            <div class="d-flex flex-column ml-5">
                <div>Total Meter: {{$slip->total_meter}}</div>
                <div class="">Actual Meter: {{$slip->actual_meter}}</div>
            </div>
            <div class="d-flex flex-column ml-auto">
                <div></div>
                <div class="mt-auto"><strong>For - Shri Sai Darshan Fabrics</strong></div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="mb-2">
    <button class="btn btn-warning" onclick="{{$slip->calculateAMeter()}}">Calculate</button>
    <button class="btn btn-success" onclick="generatePDF()">Print</button>
</div>
@endsection

@section('page-level-styles')
    <style>
        #head{
            position: relative;
            left: 3.5em;
        }
    </style>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.js" integrity="sha512-VqCeCECsaE2fYTxvPQk+OJ7+SQxzI1xZ6IqxuWd0GPKaJoeSFqeakVqNpMbx1AArieciBF71poL0dYTMiNgVxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function generatePDF() {
      // Choose the element that our invoice is rendered in.
      const element = document.getElementById("printable");
      console.log(element);
      // Choose the element and save the PDF for our user.
      html2pdf().from(element).save();
    }
    function displayModal(slipId) {
        var url = "/slips/" + slipId;
        $("#deleteSlipForm").attr('action', url);
    }
  </script>
@endsection
