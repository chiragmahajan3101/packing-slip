@extends('layouts.billGeneration.app')

@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')
<div class="card mx-auto mb-2 col-md-12">
    <div class="card-header"><h2>Create New Slip</h2></div>
        <div class="card-body">
            <form action="{{route('slips.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="party_id">Party Name</label>
                    <select name="party_id" id="party_id" class="form-control select2 @error('party_id') is-invalid @enderror">
                        <option></option>
                        @foreach ($partyInfos as $partyInfo)
                            @if($partyInfo->id == old('party_id'))
                                <option value="{{$partyInfo->id}}" selected>{{$partyInfo->company_name}}</option>
                            @else
                                <option value="{{$partyInfo->id}}">{{$partyInfo->company_name}}</option>
                            @endif
                        @endforeach
                        @error('party_id')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </select>
                </div>

                <div class="form-group">
                    <label for="serial_no">Serial No</label>
                    <input type="number"
                            class="form-control @error('serial_no') is-invalid @enderror"
                            name="serial_no"
                            value="{{ old('serial_no', ++$slip->serial_no) }}"
                            placeholder="Enter Serial Number"
                            id="serial_no">
                    @error('contact_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bill_date">Slip Date</label>
                    <input type="text"
                            class="form-control"
                            name="bill_date"
                            value="{{ old('bill_date') }}"
                            id="bill_date">
                </div>

                <div class="form-group">
                    <label for="LR_NO">Tempo No/LR No.</label>
                    <input type="text"
                            class="form-control @error('LR_NO') is-invalid @enderror"
                            name="LR_NO"
                            value="{{ old('LR_NO') }}"
                            placeholder="Enter Tempo/LR Number"
                            id="LR_NO">
                    @error('LR_NO')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="delivery_at">Delivery Address</label>
                    <input type="text"
                            class="form-control @error('delivery_at') is-invalid @enderror"
                            name="delivery_at"
                            value="{{ old('delivery_at') }}"
                            placeholder="Enter Delivery Address"
                            id="delivery_at">
                    @error('delivery_at')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="quality">Quality</label>
                    <input type="text"
                            class="form-control @error('quality') is-invalid @enderror"
                            name="quality"
                            value="{{ old('quality') }}"
                            placeholder="Enter Quality"
                            id="quality">
                    @error('delivery_at')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="fold">Fold</label>
                    <input type="number"
                            class="form-control @error('fold') is-invalid @enderror"
                            name="fold"
                            value="{{ old('fold') }}"
                            placeholder="Enter Total Folds"
                            id="fold">
                    @error('fold')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true
        });
        flatpickr("#bill_date", {
            enableTime: true,
            dateFormat: "Y-m-d H:i"
        });
    </script>
@endsection
