@extends('layouts.billGeneration.app')

@section('content')
<div class="card mb-2 col-md-5">
    <div class="card-header"><h2>Create New Bale</h2></div>
        <div class="card-body">
            <form action="{{route('slips.orderGroups.store', $slip->id)}}" method="POST">
                @csrf

                <div class="form-group">
                    <label for="bale_no">Bale No</label>
                    <input type="number"
                            class="form-control @error('bale_no') is-invalid @enderror"
                            name="bale_no"
                            value="{{ old('bale_no', ++$orderGroup->bale_no) }}"
                            placeholder="Enter Bale Number"
                            id="bale_no">
                    @error('bale_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>
@if($slip->slipGroups->count())
<div class="card mx-auto mb-2 col-md-12">
    <a href="{{route('slips.show', $slip->id)}}" class="mt-2 btn btn-success">Complete Preview Slip</a>
    <div class="card-header"><h2>Existing Bales</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Bale No</th>
                        <th>Total Meter</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($slip->slipGroups as $slipGroup)
                        <tr>
                            <td>{{$slipGroup->bale_no}}</td>
                            <td>{{$slipGroup->group_total_meter}}</td>
                            <td>
                                <a href="{{route('slips.orderGroups.edit',[$slip->id, $slipGroup->id])}}" class="btn btn-sm btn-primary mb-1">
                                    Edit
                                </a>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{$slip->id}}, {{$slipGroup->id}})" data-toggle="modal" data-target="#deleteModal">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- Delete Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Trash Party</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id="deleteBaleForm">
                        @csrf
                        @method('DELETE')
                        <div class="modal-body">
                            Are you sure, you want to Delete this Bale?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline-danger">Delete Bale</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('page-level-scripts')
<script>
    function displayModal(slipId, baleId) {
        var url = "/slips/" + slipId + "/orderGroups/" + baleId;
        $("#deleteBaleForm").attr('action', url);
    }
</script>
@endsection
