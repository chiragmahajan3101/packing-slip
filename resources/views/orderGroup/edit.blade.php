@extends('layouts.billGeneration.app')

@section('content')
<div class="card mb-2 col-md-5">
    <div class="card-header"><h2>Edit Bale</h2></div>
        <div class="card-body">
            <form action="{{route('slips.orderGroups.update', [$slip->id, $orderGroup->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="bale_no">Bale No</label>
                    <input type="number"
                            class="form-control @error('bale_no') is-invalid @enderror"
                            name="bale_no"
                            value="{{ old('bale_no', $orderGroup->bale_no) }}"
                            placeholder="Enter Bale Number"
                            id="bale_no">
                    @error('bale_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
