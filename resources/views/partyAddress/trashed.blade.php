@extends('layouts.billGeneration.app')

@section('content')
    <div class="card ">
        <div class="card-header"><h4>Trashed Parties</h4></div>
            <div class="card-body">
                <div class="media">
                    <div class="mr-4 statistics">
                        <div class="votes text-center mb-4">
                            {{-- <strong class="d-block">{{ $question->votes_count }}</strong> Slips --}}
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Email</th>
                                    <th>GST</th>
                                    <th>State</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($parties as $party)
                                    <tr>
                                        <td><a href="{{ route('partyInfos.show', $party->id) }}">{{$party->company_name}}</a></td>
                                        <td>{{$party->contact_no}}</td>
                                        <td>{{$party->email}}</td>
                                        <td>{{$party->GST_no}}</td>
                                        <td>{{$party->state}}</td>
                                        <td>
                                            <form action="{{route('partyInfos.restore', $party->id)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                            <button type="submit" class="btn btn-sm btn-primary mb-1">Restore</button>
                                            </form>
                                            <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $party->id }})" data-toggle="modal" data-target="#deleteModal">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
<!-- Delete Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteModalLabel">Trash Party</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="" method="POST" id="deletePartyForm">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-body">
                                            Are you sure, you want to Delete this Party?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-danger">Delete Party</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 ml-3">
                    {{ $parties->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
@endsection



{{--
<div class="card">
    <div class="card-header"><h2>Posts</h2></div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                @if(auth()->user()->isAdmin())
                    <th>User</th>
                @endif
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Category</th>
                    @if(!(auth()->user()->isAdmin()))
                    <th>Status</th>
                    @endif
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                    @if(auth()->user()->isAdmin())
                        <td>{{ $post->author->name }}</td>
                    @endif
                        <td><img src="{{ asset($post->image_path) }}" width="120"></td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->excerpt}}</td>
                        <td>{{$post->category->name}}</td>
                        @if(!(auth()->user()->isAdmin()))
                        <td>
                            <span class="btn btn-sm {{$post->approved ? 'btn-outline-success' : 'btn-outline-danger'}}">
                                {{$post->approved ? 'Approved' : 'Disapproved'}}
                            </span>
                        </td>
                        @endif
                        <td>
                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-primary">
                                Edit
                            </a>
                            <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $post->id }})" data-toggle="modal" data-target="#deleteModal">
                                Trash
                            </button>
                            @if(auth()->user()->isAdmin())
                            <div class="mt-2">
                                <form action="{{route('posts.status', $post->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <button type="submit" class="btn btn-sm {{$post->approved ? 'btn-outline-danger' : 'btn-outline-success'}}">
                                    @if (! $post->approved)
                                        Approve <i class="fa fa-check"></i>
                                    @else
                                        Disapprove <i class="fa fa-times"></i>
                                    @endif
                                    </button>

                                </form>
                            </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Trash post</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST" id="trashPostForm">
            @csrf
            @method('DELETE')
        <div class="modal-body">
          Are you sure, you want to Trash this Post?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline-danger">Trash Post</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<div class="mt-5">
    {{ $posts->links('vendor.pagination.bootstrap-4') }}
</div>

@endsection --}}

@section('page-level-scripts')
<script>
    function displayModal(partyId) {
        var url = "/partyInfos/" + partyId;
        $("#deletePartyForm").attr('action', url);
    }
</script>
@endsection
