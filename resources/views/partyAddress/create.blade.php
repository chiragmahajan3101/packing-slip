@extends('layouts.billGeneration.app')

@section('content')
<div class="card mb-2">
    <div class="card-header"><h2>Add New Party</h2></div>
        <div class="card-body">
            <form action="{{  route('partyInfos.store')  }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="company_name">Party Name</label>
                    <input type="text"
                            class="form-control @error('company_name') is-invalid @enderror"
                            name="company_name"
                            value="{{ old('company_name') }}"
                            placeholder="Enter Party Name"
                            id="company_name">
                    @error('company_name')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="contact_no">Contact No</label>
                    <input type="number"
                            class="form-control @error('contact_no') is-invalid @enderror"
                            name="contact_no"
                            value="{{ old('contact_no') }}"
                            placeholder="Enter Contact Number"
                            id="contact_no">
                    @error('contact_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email"
                            class="form-control @error('email') is-invalid @enderror"
                            name="email"
                            value="{{ old('email') }}"
                            placeholder="Enter Email Address"
                            id="email">
                    @error('email')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="GST_no">GST No</label>
                    <input type="text"
                            class="form-control @error('GST_no') is-invalid @enderror"
                            name="GST_no"
                            value="{{ old('GST_no') }}"
                            placeholder="Enter GST Number"
                            id="GST_no">
                    @error('GST_no')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="state">State</label>
                    <input type="text"
                            class="form-control @error('state') is-invalid @enderror"
                            name="state"
                            value="{{ old('state') }}"
                            placeholder="Enter Your State"
                            id="state">
                    @error('state')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="company_address">Address</label>
                    <textarea name="company_address"
                                id="company_address"
                                cols="30"
                                rows="5"
                                class="form-control @error('company_address') is-invalid @enderror"
                                placeholder="Enter Party Address"></textarea>
                    @error('company_address')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-outline-success mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
