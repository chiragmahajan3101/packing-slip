@extends('layouts.billGeneration.app')

@section('content')
<div class="card col-md-10 p-3">
    <div class="card-header">
        <h4>
            {{$partyInfo->company_name}}
            <span class="ml-4 btn {{$partyInfo->slip_count}}">
                {{$partyInfo->partySlips()->count()}} Slips
            </span>
        </h4>

        <table class="table">
                <tr>
                    <th>Contact</th>
                    <td>{{$partyInfo->contact_no}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$partyInfo->email}}</td>
                </tr>
                <tr>
                    <th>GST</th>
                    <td>{{$partyInfo->GST_no}}</td>
                </tr>
                <tr>
                    <th>Adress</th>
                    <td>{{$partyInfo->company_address}}</td>
                </tr>
                <tr>
                    <th>State</th>
                    <td>{{$partyInfo->state}}</td>
                </tr>
        </table>
    </div>
</div>
@if($partyInfo->partySlips()->count())
<div class="card p-3 mt-2 mb-2">
    <div class="card-body">
        @foreach ($partyInfo->partySlips as $partySlip)
        <a href="#" class="btn btn-warning mb-2 mr-2">
            <div class="d-inline">
                <table class="table d-inline">
                    <tr>
                        <th>Serial No</th>
                        <th>Slip Date</th>
                        <th>Meter</th>
                        <th>Taga</th>
                    </tr>
                    <tr>
                        <td>{{$partySlip->serial_no}}</td>
                        <td>{{explode(" ",$partySlip->bill_date)[0]}}</td>
                        <td>{{$partySlip->total_meter}} </td>
                        <td>{{ $partySlip->total_piece}}</td>
                    </tr>
                </table>
            </div>
        </a>
        @endforeach
    </div>
</div>
@endif
@endsection
